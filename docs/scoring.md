# Scoring

NOTE: Any instance of `<T>` represents your team number. For example, if you're on Team 8, `172.18.13.<T>` translates to `172.18.13.8`

# DNS

Hostname: pallet

Internal and External DNS server serving as the digital phonebook for your network. Some assembly required!

## Service Requirements

- DNS should be available internally at `192.168.<T>.12`
- DNS should be available externally at `172.18.13.<T>`
- DNS Forward lookups for the following domain names point to the corresponding IP.
- DNS Reverse lookups for the following IP Addresses point to the corresponding domain name

## Scoring Details

| DNS Address                        | IP               | Description                     |
| ---------------------------------- | ---------------- | ------------------------------- |
| `ns1.team<T>.ncaecybergames.org`   | `172.18.13.<T>`  | Public DNS Nameserver           |
| `www.team<T>.ncaecybergames.org`   | `172.18.13.<T>`  | Public Web Nameserver           |
| `shell.team<T>.ncaecybergames.org` | `172.18.14.<T>`  | Public SSH server               |
| `files.team<T>.ncaecybergames.org` | `172.18.14.<T>`  | Public File Share               |
| `www.team<T>.net`                  | `192.168.<T>.5`  | Internal Alias for Web server   |
| `db.team<T>.net`                   | `192.168.<T>.7`  | Internal Alias for MySQL server |
| `ns1.team<T>.net`                  | `192.168.<T>.12` | Internal Alias for DNS Server   |

## DNS Forward Lookup (External)

The above addresses containing .ncacybergames.org should be publicly available and will be scored through the router

- :fontawesome-solid-circle-arrow-up:{ .success } Domain names resolved correctly
- :fontawesome-solid-circle-exclamation:{ .warning } DNS Server was queried, but one or more domain names did not resolve correctly
- :fontawesome-solid-circle-arrow-down:{ .fail } Could not connect to DNS server, or all domain names were missing

## DNS Reverse Lookup (External)

The above addresses containing .ncacybergames.org should be publicly available and will be scored through the router

- :fontawesome-solid-circle-arrow-up:{ .success } IPs resolved correctly
- :fontawesome-solid-circle-exclamation:{ .warning } DNS Server was queried, but one or more IPs did not resolve correctly
- :fontawesome-solid-circle-arrow-down:{ .fail } Could not connect to DNS server, or all IPs were missing

## DNS Forward Lookup (Internal)

The above addresses containing `.team<T>.net` will be scored from inside the participant's network

- :fontawesome-solid-circle-arrow-up:{ .success } Domain names resolved correctly
- :fontawesome-solid-circle-exclamation:{ .warning } DNS Server was queried, but one or more domain names did not resolve correctly
- :fontawesome-solid-circle-arrow-down:{ .fail } Could not connect to DNS server, or all domain names were missing

## DNS Reverse Lookup (Internal)

The above addresses containing .ncacybergames.org will be scored from inside the participant's network

- :fontawesome-solid-circle-arrow-up:{ .success } IPs resolved correctly
- :fontawesome-solid-circle-exclamation:{ .warning } DNS Server was queried, but one or more IPs did not resolve correctly
- :fontawesome-solid-circle-arrow-down:{ .fail } Could not connect to DNS server, or all IPs were missing

# Web

Hostname: celadon

A suspiciously sourced PHP Web Application with a MySQL backend Database. The docs said the config file is found in /var/www/html and the MySQL database can be recovered using the setup file sccommunity.sql, but not much else. It also claimed to be "super secure" when configured correctly, whatever that means.

## Service Requirements

- Web Server stays up
- Web Server is secured using a valid HTTPS SSL Certificate for the domain team<T>.ncaecybergames.org
- MySQL connection stays available to web server
- Web content is correct (no errors) and fully functional
- Scoring User is able to log in and use site as expected
