[Black Team Introduction PDF](/assets/Feb19-Intro-Black.pdf){ .md-button }

## Infrastructure

[Latest Network Diagram](/assets/NCAECyberGames-BlueTopo.pdf){ .md-button }

- Virtual Competition Platform
    - Southwest: [https://vce1.ncaecybergames.org/](https://vce1.ncaecybergames.org/)
    - Midwest: [https://vce2.ncaecybergames.org/](https://vce2.ncaecybergames.org/)
- Scoring Engines
    - Southwest: [https://score.vce1.ncaecybergames.org/](https://score.vce1.ncaecybergames.org/)
    - Midwest: [https://score.vce2.ncaecybergames.org/](https://score.vce2.ncaecybergames.org/)
- Default VM Credentials:
    - Linux Server VMs: root / abc123
    - Kali VMs: root / toor

# Scoring Details

[Full Descriptions](/scoring){ .md-button }

| VM     | Service                 | Points | Notes                                                                                                                   |
| ------ | ----------------------- | ------ | ----------------------------------------------------------------------------------------------------------------------- |
| Router | Ping                    | 400    | Returns an ICMP Ping                                                                                                    |
| www    | httpd                   | 400    | Establishes a connection to port 80 (through router)                                                                    |
| www    | Content                 | 1200   | Verifies working web site (through router); Page is served and no errors (Full functionality)                           |
| www    | SSL                     | 1200   | Page is served with a valid SSL certificate for the domain "team<T>.ncaecybergames.org" and web correct is content      |
| shell  | ssh login               | 800    | Scoring Users are able to log in via SSH key                                                                            |
| shell  | ftp login               | 400    | Scoring users are able to log in via ftp                                                                                |
| shell  | ftp content             | 800    | Files from the list exists, can be downloaded, and contents pass an integrity check.                                    |
| shell  | ftp write               | 800    | Scoring users are able to write a file to the remote FTP server.                                                        |
| db     | mySQL read/write        | 400    | MySQL scoring user is able to connect, authenticate, read and write data                                                |
| dns    | dns fwd lookup internal | 400    | teamX.net hostnames resolve correctly. These will be scored from inside the participant's network.                      |
| dns    | dns rev lookup internal | 400    | IP resolved correctly according to the IP addressing table. These will be scored from inside the participant's network. |
| dns    | dns fwd lookup external | 400    | teamX.ncaecybergames.org hosts resolve correctly. These will be scored through the router.                              |
| dns    | dns rev lookup external | 400    | IPs resolve correctly According to the IP addressing table. These will be scored through the router.                    |
